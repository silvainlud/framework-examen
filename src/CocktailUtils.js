/**
 * Récupérer la liste d'ingrédients d'un cocktail
 * @param cocktail Cocktail cible
 * @return String[] Liste d'ingrédients
 */
export function getIngredients(cocktail) {
    let ingredients = []

    for (let i = 1; i <= 15; i++) {
        if (cocktail["strIngredient" + i] != null)
            ingredients.push(cocktail["strIngredient" + i])
    }

    return ingredients;
}

/**
 * Réalisation d'une copie d'un object cocktail
 * @param cocktail Cocktail Cible
 * @return Object copie du cocktail
 */
export function copyCocktail(cocktail) {
    return JSON.parse(JSON.stringify(cocktail))
}