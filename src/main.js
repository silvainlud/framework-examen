import { createApp } from 'vue'
import CocktailApp from './CocktailApp.vue'

createApp(CocktailApp).mount('#app')
